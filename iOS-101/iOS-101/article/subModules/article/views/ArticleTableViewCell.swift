//
//  ArticleTableViewCell.swift
//  iOS-101
//
//  Created by Леван on 23.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    
    static let nibName = "ArticleTableViewCell"
    
    func customize(article: ArticleModel) {
        titleLabel.text = article.title
        bodyLabel.text = article.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
