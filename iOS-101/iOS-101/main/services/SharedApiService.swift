//
//  SharedApiService.swift
//  iOS-101
//
//  Created by Леван on 30.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Foundation


class SharedApiService: NSObject {
    
    static let sharedInstance: SharedApiService = { SharedApiService() } ()
    
    private(set) var articleService: ArticleService
    private(set) var commentService: CommentService
    
    private override init() {
        
        self.articleService = ArticleService()
        self.commentService = CommentService()

    }
}
