//
//  AllArticlesDataProvider.swift
//  iOS-101
//
//  Created by Леван on 30.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Foundation


protocol AllArticlesDataProviderDelegate: class {
    func articlesDataLoaded()
    func articlesDataHasError(error: String)
}

class AllArticlesDataProvider: NSObject {
    
    var articles: [ArticleModel] = []
    
    weak var delegate: AllArticlesDataProviderDelegate?
    
    func refresh () {
        
        articles = []
        loadArticles()
        
    }
    
    
    func loadArticles() {
        
        SharedApiService.sharedInstance.articleService.posts(limit: 100, skip: 0) { (articlesResponse, errors) in
            
            if let error = errors {
                
                self.delegate?.articlesDataHasError(error: error)
                return
                
            } else if let articles:[ArticleModel] = articlesResponse {
                
                self.articles.append(contentsOf: articles)
                self.delegate?.articlesDataLoaded()
                return
            }
            
            self.delegate?.articlesDataLoaded()
            
        }
        
    }
    
    
}
