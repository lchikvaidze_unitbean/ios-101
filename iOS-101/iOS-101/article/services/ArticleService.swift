//
//  ArticleService.swift
//  iOS-101
//
//  Created by Леван on 30.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Alamofire
import SwiftyJSON
import ObjectMapper


typealias ArticlesPostCompletion = (_ article: [ArticleModel]?, _ error: String?) -> Void

class ArticleService: TypicodeApiService {
    
    func posts (limit: Int, skip: Int, completion: @escaping ArticlesPostCompletion) {
        
        let url = host + "/posts"
        
        var params: [String: AnyObject] = [:]
        
        params["limit"] = limit as AnyObject
        params["skip"]  = skip  as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                } else if let articles = responseData!.arrayObject {
                    let articleModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
                    
                    completion(articleModels, nil)
                    return
                }
                completion(nil, "Ошибка загрузки каталога")
        }
    }
}
