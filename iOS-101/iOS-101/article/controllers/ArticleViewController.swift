//
//  ArticleViewController.swift
//  iOS-101
//
//  Created by Леван on 23.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ArticleDataProviderDelegate {
    
    
    // MARK: - PARAMS
    
    static let nibName = "ArticleViewController"

    @IBOutlet weak var tableView: UITableView!
    
    var dataProvider: ArticleDataProvider = ArticleDataProvider()
    
    // MARK: - View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.delegate = self
        
        customizeTableView()
        
        dataProvider.loadData()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    
    func customizeTableView() {
        tableView.register(UINib(nibName: ArticleTitleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleTitleTableViewCell.nibName)
        
        
        tableView.register(UINib(nibName: ArticleContentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleContentTableViewCell.nibName)
        
        tableView.register(UINib(nibName: CommentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CommentTableViewCell.nibName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1{
            return 1
        }else{
            return dataProvider.comments.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTitleTableViewCell.nibName) as! ArticleTitleTableViewCell
            cell.customize(article: dataProvider.article)
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleContentTableViewCell.nibName) as! ArticleContentTableViewCell
            cell.customize(article: dataProvider.article)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.nibName) as! CommentTableViewCell
            cell.customize(comment: dataProvider.comments[indexPath.row])
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 126
        }else if indexPath.section == 1{
            return 219
        }else{
            return 79
        }
    }
    
    
    // MARK: - ArticleDataProviderDelegate
    
    func articleDataDidLoad() {
        tableView.reloadData()
    }
    
    func articleDataHasError(error: String) {
         
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
