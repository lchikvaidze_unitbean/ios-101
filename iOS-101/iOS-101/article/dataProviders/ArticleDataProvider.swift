//
//  ArticleDataProvider.swift
//  iOS-101
//
//  Created by Леван on 31.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Foundation

protocol ArticleDataProviderDelegate: class {
    
    func articleDataDidLoad()
    func articleDataHasError(error: String)
    
}


class ArticleDataProvider: NSObject {
    
    var article: ArticleModel = ArticleModel()
    var comments: [CommentModel] = []

    weak var delegate: ArticleDataProviderDelegate?

    func refresh() {
        comments = []
        loadData()
    }

    func loadData() {

        if article.id != nil {

            SharedApiService.sharedInstance.commentService.comments(postID: article.id!, completion: { (commentsResponse, errors) in

                if let error = errors {
                    self.delegate?.articleDataHasError(error: error)
                    return

                } else if let commentsArray: [CommentModel] = commentsResponse {
                    self.comments.append(contentsOf: commentsArray)
                    self.delegate?.articleDataDidLoad()
                    return
                }

                self.delegate?.articleDataDidLoad()
            })

        }


    }
}

