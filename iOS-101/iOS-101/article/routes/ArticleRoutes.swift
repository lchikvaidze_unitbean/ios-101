//
//  ArticleRoutes.swift
//  iOS-101
//
//  Created by Леван on 24.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Foundation
import UIKit

class ArticleRoutes {
    static func showArticle(article: ArticleModel, fromVc: UIViewController) {
        let articleVC = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticleViewController.nibName) as! ArticleViewController
        
        articleVC.dataProvider.article = article
        
        fromVc.navigationController?.pushViewController(articleVC, animated: true)
        
        
    }
}
