//
//  CommentTableViewCell.swift
//  iOS-101
//
//  Created by Леван on 23.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
   
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    static let nibName = "CommentTableViewCell"
    
    func customize(comment: CommentModel) {
        nameLabel.text = comment.name
        commentLabel.text = comment.body
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
