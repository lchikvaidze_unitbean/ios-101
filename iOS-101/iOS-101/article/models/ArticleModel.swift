//
//  ArticleModel.swift
//  iOS-101
//
//  Created by Леван on 30.01.2018.
//  Copyright © 2018 Levan Chikvaidze. All rights reserved.
//

import Foundation
import ObjectMapper

class ArticleModel: NSObject, Mappable {
    
    var id: Int?
    var userId: Int?
    var title: String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        
        id             <- map["id"]
        userId         <- map["userId"]
        title          <- map["title"]
        body           <- map["body"]
        
    }
}
